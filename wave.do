onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /cpu/COMP_REGF/clk
add wave -noupdate /cpu/COMP_REGF/reset
add wave -noupdate -radix hexadecimal /cpu/COMP_IF/if2id_out
add wave -noupdate -radix hexadecimal /cpu/COMP_ID/id2ex_out
add wave -noupdate -radix hexadecimal /cpu/COMP_EX/ex2mem_out
add wave -noupdate -radix hexadecimal /cpu/COMP_MEM/mem2wb_out
add wave -noupdate -radix hexadecimal /cpu/COMP_REGF/reg_reg
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {0 ps} 0}
quietly wave cursor active 0
configure wave -namecolwidth 150
configure wave -valuecolwidth 70
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ns
update
WaveRestoreZoom {0 ps} {237074 ps}
