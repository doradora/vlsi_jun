onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /predictor_vhd_tst/addr2if_out
add wave -noupdate /predictor_vhd_tst/addr_from_wb
add wave -noupdate /predictor_vhd_tst/addr_in
add wave -noupdate /predictor_vhd_tst/addr_to_wb
add wave -noupdate /predictor_vhd_tst/clk
add wave -noupdate /predictor_vhd_tst/flag2if_out
add wave -noupdate /predictor_vhd_tst/jump_wb
add wave -noupdate /predictor_vhd_tst/reset
add wave -noupdate /predictor_vhd_tst/i1/pred_reg
add wave -noupdate /predictor_vhd_tst/i1/pred_next
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {0 ps} 0}
quietly wave cursor active 0
configure wave -namecolwidth 150
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ns
update
WaveRestoreZoom {199050 ps} {200050 ps}
