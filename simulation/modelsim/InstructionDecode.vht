LIBRARY ieee;                                               
USE ieee.std_logic_1164.all; 
use work.types.all;                               

ENTITY InstructionDecode_vhd_tst IS
END InstructionDecode_vhd_tst;
ARCHITECTURE InstructionDecode_arch OF InstructionDecode_vhd_tst IS
-- constants                                                 
-- signals                                                   
SIGNAL clk : STD_LOGIC;
SIGNAL ex_flush_in : STD_LOGIC;
SIGNAL hctrl_regs_in : regs_t;
SIGNAL hctrl_stall_in : STD_LOGIC;
SIGNAL id2ex_out : instruction;
SIGNAL if2id_in : instruction;
SIGNAL im_current_instr_in : STD_LOGIC_VECTOR(31 DOWNTO 0);
SIGNAL im_current_pc_out : STD_LOGIC_VECTOR(31 DOWNTO 0);
SIGNAL regfile_hctrl_regs_out : regs_t;
SIGNAL regfile_regs_in : regs_t;
SIGNAL reset : STD_LOGIC;
COMPONENT InstructionDecode
	PORT (
	clk : IN STD_LOGIC;
	ex_flush_in : IN STD_LOGIC;
	hctrl_regs_in : IN regs_t;
	hctrl_stall_in : IN STD_LOGIC;
	id2ex_out : OUT instruction;
	if2id_in : IN instruction;
	im_current_instr_in : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
	im_current_pc_out : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
	regfile_hctrl_regs_out : OUT regs_t;
	regfile_regs_in : IN regs_t;
	reset : IN STD_LOGIC
	);
END COMPONENT;
BEGIN
	i1 : InstructionDecode
	PORT MAP (
-- list connections between master ports and signals
	clk => clk,
	ex_flush_in => ex_flush_in,
	hctrl_regs_in => hctrl_regs_in,
	hctrl_stall_in => hctrl_stall_in,
	id2ex_out => id2ex_out,
	if2id_in => if2id_in,
	im_current_instr_in => im_current_instr_in,
	im_current_pc_out => im_current_pc_out,
	regfile_hctrl_regs_out => regfile_hctrl_regs_out,
	regfile_regs_in => regfile_regs_in,
	reset => reset
	);

PROCESS                                               
	variable clk_next : std_logic := '1';
BEGIN
	loop
		clk <= clk_next;
		clk_next := not clk_next;
		wait for 5 ns;
	end loop;      
END PROCESS;  
	
PROCESS                                                                              
BEGIN
	im_current_instr_in <= "00110000011000100000000000000101";--ADDI Rd->reg3 Rs1->reg2 imm16->5
	if2id_in.currentPC <= "00000000000000000000000000000111";
	reset <= '1';
	wait for 30 ns;
	reset <= '0';
	
	hctrl_stall_in <= '1';
	im_current_instr_in <= "10100000000000110001000000000101";--BEQ Rs1->reg3 Rs2->reg2 imm16->0+5
	wait for 30 ns;
	hctrl_stall_in <= '0';

	--regfile_regs_in.rd.myValue <= "00000000000000000000000000000111";

wait for 170 ns;                                                        
END PROCESS;                                          
END InstructionDecode_arch;
