onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -radix hexadecimal /predictor_vhd_tst/i1/clk
add wave -noupdate -radix hexadecimal /predictor_vhd_tst/i1/reset
add wave -noupdate -radix hexadecimal /predictor_vhd_tst/i1/if_addr_from_in
add wave -noupdate -radix hexadecimal /predictor_vhd_tst/i1/if_addr_to_out
add wave -noupdate -radix hexadecimal /predictor_vhd_tst/i1/if_branch_out
add wave -noupdate -radix hexadecimal /predictor_vhd_tst/i1/wb_addr_from_in
add wave -noupdate -radix hexadecimal /predictor_vhd_tst/i1/wb_addr_to_in
add wave -noupdate -radix hexadecimal /predictor_vhd_tst/i1/wb_jump_in
add wave -noupdate -radix hexadecimal /predictor_vhd_tst/i1/pred_reg
add wave -noupdate -radix hexadecimal /predictor_vhd_tst/i1/pred_next
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {0 ps} 0}
quietly wave cursor active 0
configure wave -namecolwidth 150
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ns
update
WaveRestoreZoom {0 ps} {128 ns}
