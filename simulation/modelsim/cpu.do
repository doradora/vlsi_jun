onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -radix hexadecimal /cpu_vhd_tst/clk
add wave -noupdate -radix hexadecimal /cpu_vhd_tst/reset
add wave -noupdate -radix hexadecimal /cpu_vhd_tst/i1/COMP_IF/if2id_out
add wave -noupdate -radix hexadecimal /cpu_vhd_tst/i1/COMP_ID/if2id_in
add wave -noupdate -radix hexadecimal /cpu_vhd_tst/i1/COMP_ID/id2ex_out
add wave -noupdate -radix hexadecimal /cpu_vhd_tst/i1/COMP_EX/id2ex_in
add wave -noupdate -radix hexadecimal /cpu_vhd_tst/i1/COMP_EX/ex2mem_out
add wave -noupdate -radix hexadecimal /cpu_vhd_tst/i1/COMP_MEM/ex2mem_in
add wave -noupdate -radix hexadecimal /cpu_vhd_tst/i1/COMP_MEM/mem2wb_out
add wave -noupdate -radix hexadecimal /cpu_vhd_tst/i1/COMP_WB/mem2wb_in
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {1419 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 150
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ns
update
WaveRestoreZoom {0 ps} {64 ns}
