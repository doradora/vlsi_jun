onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /cpu_vhd_tst/clk
add wave -noupdate /cpu_vhd_tst/reset
add wave -noupdate -radix hexadecimal /cpu_vhd_tst/clk
add wave -noupdate -radix hexadecimal /cpu_vhd_tst/reset
add wave -noupdate -radix hexadecimal /cpu_vhd_tst/i1/COMP_IF/pc_reg
add wave -noupdate -radix hexadecimal /cpu_vhd_tst/i1/COMP_IF/pc_next
add wave -noupdate -radix hexadecimal /cpu_vhd_tst/i1/COMP_ID/if2id_in
add wave -noupdate /cpu_vhd_tst/i1/COMP_ID/regfile_regs_in
add wave -noupdate /cpu_vhd_tst/i1/COMP_ID/id2ex_out
add wave -noupdate -radix hexadecimal /cpu_vhd_tst/i1/COMP_ID/im_current_pc_out
add wave -noupdate -radix hexadecimal /cpu_vhd_tst/i1/COMP_ID/im_current_instr_in
add wave -noupdate -radix hexadecimal /cpu_vhd_tst/i1/COMP_ID/instruction_reg
add wave -noupdate -radix hexadecimal /cpu_vhd_tst/i1/COMP_ID/instruction_next
add wave -noupdate -radix hexadecimal /cpu_vhd_tst/i1/COMP_IC/id_current_pc_in
add wave -noupdate -radix hexadecimal /cpu_vhd_tst/i1/COMP_IC/id_current_instr_out
add wave -noupdate /cpu_vhd_tst/i1/COMP_REGF/reg_reg
add wave -noupdate -expand /cpu_vhd_tst/i1/COMP_EX/ex2mem_out
add wave -noupdate /cpu_vhd_tst/i1/COMP_EX/instruction_reg
add wave -noupdate /cpu_vhd_tst/i1/COMP_EX/instruction_next
add wave -noupdate /cpu_vhd_tst/i1/COMP_EX/if_correct_addr_out
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {101196 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 150
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ns
update
WaveRestoreZoom {98429 ps} {130429 ps}
