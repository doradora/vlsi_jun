LIBRARY ieee;                                               
USE ieee.std_logic_1164.all;                                
USE work.types.all;

ENTITY InstructionFetch_vhd_tst IS
END InstructionFetch_vhd_tst;
ARCHITECTURE InstructionFetch_arch OF InstructionFetch_vhd_tst IS
-- constants                                                 
-- signals                                                   
SIGNAL clk : STD_LOGIC;
SIGNAL ex_correct_addr_in : STD_LOGIC_VECTOR(31 DOWNTO 0);
SIGNAL ex_flush_in : STD_LOGIC;
SIGNAL hctrl_stall_in : STD_LOGIC;
SIGNAL if2id_out : instruction;
SIGNAL pred_addr_from_out : STD_LOGIC_VECTOR(31 DOWNTO 0);
SIGNAL pred_addr_to_in : STD_LOGIC_VECTOR(31 DOWNTO 0);
SIGNAL pred_branch_in : STD_LOGIC;
SIGNAL reset : STD_LOGIC;
COMPONENT InstructionFetch
	PORT (
	clk : IN STD_LOGIC;
	ex_correct_addr_in : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
	ex_flush_in : IN STD_LOGIC;
	hctrl_stall_in : IN STD_LOGIC;
	if2id_out : OUT instruction;
	pred_addr_from_out : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
	pred_addr_to_in : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
	pred_branch_in : IN STD_LOGIC;
	reset : IN STD_LOGIC
	);
END COMPONENT;
BEGIN
	i1 : InstructionFetch
	PORT MAP (
-- list connections between master ports and signals
	clk => clk,
	ex_correct_addr_in => ex_correct_addr_in,
	ex_flush_in => ex_flush_in,
	hctrl_stall_in => hctrl_stall_in,
	if2id_out => if2id_out,
	pred_addr_from_out => pred_addr_from_out,
	pred_addr_to_in => pred_addr_to_in,
	pred_branch_in => pred_branch_in,
	reset => reset
	);
	
PROCESS                                               
	variable clk_next : std_logic := '1';
BEGIN
	loop
		clk <= clk_next;
		clk_next := not clk_next;
		wait for 5 ns;
	end loop;      
END PROCESS;                                           


PROCESS                                              

BEGIN  
                                                       
   ex_correct_addr_in <= "00000000000000000000000000000101";
	pred_addr_to_in <= "00000000000000000000000000110011";
	hctrl_stall_in <= '0';
	ex_flush_in <= '0';
	pred_branch_in <= '0';
	
	reset <= '1';
	wait for 15 ns;--15ns
	reset <= '0';
	
	wait for 20 ns;--35ns
	
	pred_branch_in <= '1';
	wait for 5 ns;--40ns
	pred_branch_in <= '0';
	
	wait for 15 ns;--55ns
	
	ex_flush_in <= '1';
	wait for 5 ns;--60ns
	ex_flush_in <= '0';
	
	wait for 10 ns;--70ns
	
	hctrl_stall_in <= '1';
	wait for 40 ns;--110ns
	hctrl_stall_in <= '0';
	
	wait for 90 ns;                                         
END PROCESS;    
	
END InstructionFetch_arch;
