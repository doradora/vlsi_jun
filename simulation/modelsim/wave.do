onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -radix hexadecimal /projekat/COMP_CPU/COMP_IF/clk
add wave -noupdate -radix hexadecimal /projekat/COMP_CPU/COMP_IF/reset
add wave -noupdate -radix hexadecimal -childformat {{/projekat/COMP_CPU/COMP_IF/if2id_out.currentPC -radix hexadecimal} {/projekat/COMP_CPU/COMP_IF/if2id_out.nextPC -radix hexadecimal} {/projekat/COMP_CPU/COMP_IF/if2id_out.branch -radix hexadecimal} {/projekat/COMP_CPU/COMP_IF/if2id_out.flush -radix hexadecimal} {/projekat/COMP_CPU/COMP_IF/if2id_out.halt -radix hexadecimal} {/projekat/COMP_CPU/COMP_IF/if2id_out.opcode -radix hexadecimal} {/projekat/COMP_CPU/COMP_IF/if2id_out.immediate5 -radix hexadecimal} {/projekat/COMP_CPU/COMP_IF/if2id_out.immediate32 -radix hexadecimal} {/projekat/COMP_CPU/COMP_IF/if2id_out.registers -radix hexadecimal}} -subitemconfig {/projekat/COMP_CPU/COMP_IF/if2id_out.currentPC {-radix hexadecimal} /projekat/COMP_CPU/COMP_IF/if2id_out.nextPC {-radix hexadecimal} /projekat/COMP_CPU/COMP_IF/if2id_out.branch {-radix hexadecimal} /projekat/COMP_CPU/COMP_IF/if2id_out.flush {-radix hexadecimal} /projekat/COMP_CPU/COMP_IF/if2id_out.halt {-radix hexadecimal} /projekat/COMP_CPU/COMP_IF/if2id_out.opcode {-radix hexadecimal} /projekat/COMP_CPU/COMP_IF/if2id_out.immediate5 {-radix hexadecimal} /projekat/COMP_CPU/COMP_IF/if2id_out.immediate32 {-radix hexadecimal} /projekat/COMP_CPU/COMP_IF/if2id_out.registers {-radix hexadecimal}} /projekat/COMP_CPU/COMP_IF/if2id_out
add wave -noupdate -radix hexadecimal /projekat/COMP_CPU/COMP_ID/id2ex_out
add wave -noupdate -radix hexadecimal /projekat/COMP_CPU/COMP_EX/ex2mem_out
add wave -noupdate -radix hexadecimal /projekat/COMP_CPU/COMP_MEM/mem2wb_out
add wave -noupdate -radix hexadecimal /projekat/COMP_CPU/COMP_REGF/reg_reg
add wave -noupdate /projekat/COMP_CPU/COMP_REGF/id_regs_in
add wave -noupdate /projekat/COMP_DC/reset
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {36135 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 150
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ns
update
WaveRestoreZoom {0 ps} {11411 ps}
