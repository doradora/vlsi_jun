transcript on
if {[file exists rtl_work]} {
	vdel -lib rtl_work -all
}
vlib rtl_work
vmap work rtl_work

vcom -93 -work work {C:/Users/ana/Desktop/vlsi_jun/src/types.vhd}
vcom -93 -work work {C:/Users/ana/Desktop/vlsi_jun/src/wb.vhd}
vcom -93 -work work {C:/Users/ana/Desktop/vlsi_jun/src/mem.vhd}
vcom -93 -work work {C:/Users/ana/Desktop/vlsi_jun/src/EX.vhd}
vcom -93 -work work {C:/Users/ana/Desktop/vlsi_jun/src/predictor.vhd}
vcom -93 -work work {C:/Users/ana/Desktop/vlsi_jun/src/ID.vhd}
vcom -93 -work work {C:/Users/ana/Desktop/vlsi_jun/src/IF.vhd}
vcom -93 -work work {C:/Users/ana/Desktop/vlsi_jun/src/regfile.vhd}
vcom -93 -work work {C:/Users/ana/Desktop/vlsi_jun/src/ICache.vhd}
vcom -93 -work work {C:/Users/ana/Desktop/vlsi_jun/src/DCache.vhd}
vcom -93 -work work {C:/Users/ana/Desktop/vlsi_jun/src/cpu.vhd}

vcom -93 -work work {C:/Users/ana/Desktop/vlsi_jun/simulation/modelsim/CPU.vht}

vsim -t 1ps -L altera -L lpm -L sgate -L altera_mf -L altera_lnsim -L cycloneii -L rtl_work -L work -voptargs="+acc"  CPU_vhd_tst

add wave *
view structure
view signals
run 200 ns
