onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /instructiondecode_vhd_tst/clk
add wave -noupdate /instructiondecode_vhd_tst/reset
add wave -noupdate /instructiondecode_vhd_tst/ex_flush_in
add wave -noupdate /instructiondecode_vhd_tst/hctrl_regs_in
add wave -noupdate /instructiondecode_vhd_tst/hctrl_stall_in
add wave -noupdate /instructiondecode_vhd_tst/id2ex_out
add wave -noupdate -expand /instructiondecode_vhd_tst/if2id_in
add wave -noupdate /instructiondecode_vhd_tst/im_current_instr_in
add wave -noupdate /instructiondecode_vhd_tst/im_current_pc_out
add wave -noupdate /instructiondecode_vhd_tst/regfile_hctrl_regs_out
add wave -noupdate /instructiondecode_vhd_tst/regfile_regs_in
add wave -noupdate /instructiondecode_vhd_tst/i1/instruction_reg
add wave -noupdate /instructiondecode_vhd_tst/i1/instruction_next
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {19742 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 150
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ns
update
WaveRestoreZoom {0 ps} {128 ns}
