LIBRARY ieee;                                               
USE ieee.std_logic_1164.all;                                

ENTITY InstrMem_vhd_tst IS
END InstrMem_vhd_tst;
ARCHITECTURE InstrMem_arch OF InstrMem_vhd_tst IS
-- constants                                                 
-- signals                                                   
SIGNAL id_current_instr_out : STD_LOGIC_VECTOR(31 DOWNTO 0);
SIGNAL id_current_pc_in : STD_LOGIC_VECTOR(31 DOWNTO 0);
COMPONENT InstrMem
	PORT (
	id_current_instr_out : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
	id_current_pc_in : IN STD_LOGIC_VECTOR(31 DOWNTO 0)
	);
END COMPONENT;
BEGIN
	i1 : InstrMem
	PORT MAP (
-- list connections between master ports and signals
	id_current_instr_out => id_current_instr_out,
	id_current_pc_in => id_current_pc_in
	);
	
PROCESS                                               
	
BEGIN

		wait for 200 ns;
     
END PROCESS;   
                              
END InstrMem_arch;
