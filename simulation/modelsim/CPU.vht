LIBRARY ieee;                                               
USE ieee.std_logic_1164.all;                                

ENTITY CPU_vhd_tst IS
END CPU_vhd_tst;
ARCHITECTURE CPU_arch OF CPU_vhd_tst IS
-- constants                                                 
-- signals                                                   
SIGNAL clk : STD_LOGIC;
SIGNAL reset : STD_LOGIC;
COMPONENT CPU
	PORT (
	clk : IN STD_LOGIC;
	reset : IN STD_LOGIC
	);
END COMPONENT;
BEGIN
	i1 : CPU
	PORT MAP (
-- list connections between master ports and signals
	clk => clk,
	reset => reset
	);

PROCESS                                               
	variable clk_next : std_logic := '1';
BEGIN
	reset <= '1';
	wait for 5 ns;--15ns
	reset <= '0';
	--wait for 195 ns;--200ns
	
	loop
		clk <= clk_next;
		clk_next := not clk_next;
		wait for 5 ns;
	end loop;      
END PROCESS; 
	
END CPU_arch;
