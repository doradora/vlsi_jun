LIBRARY ieee;                                               
USE ieee.std_logic_1164.all;                                

ENTITY Predictor_vhd_tst IS
END Predictor_vhd_tst;
ARCHITECTURE Predictor_arch OF Predictor_vhd_tst IS
-- constants                                                 
-- signals                                                   
SIGNAL clk : STD_LOGIC;
SIGNAL if_addr_from_in : STD_LOGIC_VECTOR(31 DOWNTO 0);
SIGNAL if_addr_to_out : STD_LOGIC_VECTOR(31 DOWNTO 0);
SIGNAL if_branch_out : STD_LOGIC;
SIGNAL reset : STD_LOGIC;
SIGNAL wb_addr_from_in : STD_LOGIC_VECTOR(31 DOWNTO 0);
SIGNAL wb_addr_to_in : STD_LOGIC_VECTOR(31 DOWNTO 0);
SIGNAL wb_jump_in : STD_LOGIC;
COMPONENT Predictor
	PORT (
	clk : IN STD_LOGIC;
	if_addr_from_in : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
	if_addr_to_out : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
	if_branch_out : OUT STD_LOGIC;
	reset : IN STD_LOGIC;
	wb_addr_from_in : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
	wb_addr_to_in : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
	wb_jump_in : IN STD_LOGIC
	);
END COMPONENT;
BEGIN
	i1 : Predictor
	PORT MAP (
-- list connections between master ports and signals
	clk => clk,
	if_addr_from_in => if_addr_from_in,
	if_addr_to_out => if_addr_to_out,
	if_branch_out => if_branch_out,
	reset => reset,
	wb_addr_from_in => wb_addr_from_in,
	wb_addr_to_in => wb_addr_to_in,
	wb_jump_in => wb_jump_in
	);
PROCESS                                    
variable clk_next : std_logic := '1';                                  
BEGIN
clk <= clk_next;
clk_next := not clk_next;
wait for 5 ns;
END PROCESS;

PROCESS
BEGIN
	reset <= '1';
	wait for 30 ns;
	reset <= '0';

	--1.ciklus

	--wb ka prediktoru
	wb_addr_from_in <= "00000000000000000000000000000001";  --u ulaz 1 treba da napise 3
	wb_addr_to_in <= "00000000000000000000000000000011";
	wb_jump_in <= '1';
	wait for 10 ns;

	--2.ciklus

	--wb ka prediktoru
	wb_addr_from_in <= "00000000000000000000000000000010"; --nista
	wb_addr_to_in <= "00000000000000000000000000000011";
	wb_jump_in <= '0';

	--if ka prediktoru
	if_addr_from_in <= "00000000000000000000000000000001"; --treba da vrati 3 
	wait for 10 ns;

	--3.ciklus

	--wb ka pred.
	wb_addr_from_in <= "00000000000000000000000000000011"; --
	wb_addr_to_in <= "00000000000000000000000000000011";
	wb_jump_in <= '0';

	--if ka pred.
	if_addr_from_in <= "00000000000000000000000000000010";
	wait for 10 ns;

	--4.ciklus

	--wb ka prediktoru
	wb_addr_from_in <= "00000000000000000000000000000100";
	wb_addr_to_in <= "00000000000000000000000000001111";
	wb_jump_in <= '1';

	--if ka pred.
	if_addr_from_in <= "00000000000000000000000000000100";

	wait for 10 ns;

	--5.ciklus

	--wb ka
	wb_addr_from_in <= "00000000000000000000000000000100";
	wb_addr_to_in <= "00000000000000000000000000001111";
	wb_jump_in <= '0';

	--if ka
	if_addr_from_in <= "00000000000000000000000000000100";
	wait for 10 ns;

	--6.ciklus
	wb_addr_from_in <= "00000000000000000000000000000100";
	wb_addr_to_in <= "00000000000000000000000000001111";
	wb_jump_in <= '0';

	--if ka
	if_addr_from_in <= "00000000000000000000000000000100"; --treba da ne bude predikcije, flag 0;
	wait for 10 ns;

	wait for 110 ns;

END PROCESS;                                         
END Predictor_arch;
