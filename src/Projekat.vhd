library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.types.all;

entity Projekat is
end entity;

architecture Projekat_impl of Projekat is
	-- signals                                                   
	SIGNAL clk   : STD_LOGIC;
	SIGNAL reset : STD_LOGIC;

	signal wb_halt_out         : flag;
	signal mem_addr_dm         : addr;
	signal mem_word_dm         : word;
	signal mem_flag_dm         : flag;
	signal mem_RW_dm           : flag;
	signal dm_word_mem         : word;
	signal id_current_pc_im    : addr;
	signal im_current_instr_id : word;

begin
	COMP_CPU : entity work.CPU
		port map(
			clk_in      => clk,
			reset       => reset,
			im_addr_out => id_current_pc_im,
			im_data_in  => im_current_instr_id,

			--Data Cache
			dm_addr_out => mem_addr_dm,
			dm_data_out => mem_word_dm,
			dm_data_in  => dm_word_mem,
			dm_RW_out   => mem_RW_dm,
			dm_flag_out => mem_flag_dm,
			halt_dm     => wb_halt_out
		);

	COMP_IC : entity work.InstrMem
		port map(
			--reset                => reset,
			id_current_pc_in     => id_current_pc_im,
			id_current_instr_out => im_current_instr_id
		);
	COMP_DC : entity work.DataMem
		port map(
			--reset        => reset,
			halt_in      => wb_halt_out,
			mem_addr_in  => mem_addr_dm,
			mem_word_in  => mem_word_dm,
			mem_flag_in  => mem_flag_dm,
			mem_RW_in    => mem_RW_dm,
			mem_word_out => dm_word_mem
		);

	PROCESS
		variable clk_next : std_logic := '1';
	BEGIN
		reset <= '1';
		wait for 15 ns;                 --15ns
		reset <= '0';
		--wait for 195 ns;--200ns

		loop
			clk      <= clk_next;
			clk_next := not clk_next;
			wait for 5 ns;
		end loop;
	END PROCESS;

end architecture;