library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_signed.all;
use ieee.numeric_std.all;
use ieee.std_logic_textio.all;
use work.types.all;
use std.textio.all;

entity InstrMem is
	port(
		--reset : in flag;
		
		--ID
		id_current_pc_in     : in  addr;
		id_current_instr_out : out word
	);
end entity;

architecture InstrMem_impl of InstrMem is
	--signal instr_cache : instr_cache_t;
	
	impure function init_cache return instr_cache_t is
		file input_file : text;
		variable input_line             : line;
		variable input_addr, input_data : addr; -- address and data read from input file
		variable tmp_mem                : instr_cache_t := (others => (others => '0'));
	begin
		-- Init instruction cache from file

		file_open(input_file, file_path, read_mode);

		readline(input_file, input_line); --Skip first line with PC register

		loop
			exit when endfile(input_file);

			readline(input_file, input_line); --Read the line from the file
			hread(input_line, input_addr); --Read first word (adress)
			read(input_line, input_data); --Read second word (value for memory location )

			tmp_mem(to_integer(unsigned(input_addr))) := input_data;

		end loop;

		file_close(input_file);         --after reading all the lines close the file

		return tmp_mem;
	end;

	shared variable instr_cache : instr_cache_t := init_cache;

begin

--	process(reset)
--	begin
--		if (reset = '1') then
--			instr_cache <= init_cache;
--		end if;
--	end process;
	
	process(id_current_pc_in)
	begin
		id_current_instr_out <= instr_cache(to_integer(unsigned(id_current_pc_in)));
	end process;

end architecture;