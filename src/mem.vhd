library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_signed.all;
use ieee.numeric_std.all;
use work.types.all;

entity Mem is
	port(
		clk           : in  flag;
		reset         : in  flag;

		--HAZARD, da bi radilo prosledjivanje
		hctrl_reg_out : out reg_t;

		--DATA MEMORIJA
		dm_addr_out   : out addr;       --adresa na koju treba da se pise
		dm_word_out   : out word;       --rec koja treba da se pise u memoriju
		dm_flag_out   : out flag;       --da li je memorija potrebna
		dm_RW_out     : out flag;       --0 citanje, 1 pisanje u mem.
		dm_word_in    : in  word;       --rec koja nam je stigla iz memorije

		--EX
		ex2mem_in     : in  instruction;

		--WB
		mem2wb_out    : out instruction
	);
end entity;

architecture mem_impl of Mem is
	signal instruction_reg, instruction_next : instruction;
begin
	process(clk, reset)
	begin
		if (reset = '1') then
			instruction_reg <= init;
		elsif (rising_edge(clk)) then
			instruction_reg <= instruction_next;
		end if;
	end process;

	process(dm_word_in, ex2mem_in, instruction_reg)
		variable instr : instruction;
	begin
		instr       := ex2mem_in;
		dm_addr_out <= (others => '0'); --saljemo adresu memoriji
		dm_word_out <= (others => '0');
		--dm_addr_out <= instr.registers.rd.myValue; --saljemo adresu memoriji
		--dm_word_out <= instr.registers.rs2.myValue; --saljemo podatak memoriji, treba nam za STORE, LOAD ne koristi ovu rec
		dm_flag_out <= '0';             --za pocetak, flag je negativan i ne koristimo mem dok ne utvrdimo da li nam je operacija load ili store
		dm_RW_out   <= '0';             --stavljamo smer na citanje

		if (instr.opcode = OPCODE_STORE or instr.opcode = OPCODE_LOAD) then
			if (instr.flush = '1') then
				--memorija se ne koristi,
				--instrukcija je flasovana
				dm_flag_out <= '0';
			else
				dm_flag_out <= '1';
				dm_addr_out <= instr.registers.rd.myValue;
				if (instr.opcode = OPCODE_STORE) then
					dm_RW_out <= '1';   --pisemo u memoriju !! vazan fleg
					dm_word_out <= instr.registers.rs2.myValue;
				else
					--load
					dm_RW_out                  <= '0'; --citamo iz memorije
					instr.registers.rd.myValue := dm_word_in; --pisemo u rd da bismo prosledili WB fazi
					instr.registers.rd.hazard  := '1'; --nisam siguran, ex je verovatno ovo stavila na 1...
					--jeste ex stavila na 1 ali mi treba da kazemo da je podatak sad spreman pomocu:
					instr.registers.rd.isRead  := '1';
				end if;
			end if;
		end if;
		

		instruction_next <= instr;

		--on ce uvek da salje kontroleru hazarda rd registar
		--ali mi necemo uzimati u obzir tu vrednost ako je hazard = '0', ja mislim
		hctrl_reg_out <= instr.registers.rd;

	end process;

	mem2wb_out <= instruction_reg;

end architecture;