library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_signed.all;
use ieee.numeric_std.all;
use work.types.all;

entity Hazard_Control is
	port(
		--ID
		id_regs_in		 : in  regs_t;
		id_regs_out 	 : out regs_t;

		--EX
		ex_reg_in       : in  reg_t;

		--MEM
		mem_reg_in      : in  reg_t;

		--WB
		wb_reg_in       : in  reg_t;

		--ID&IF
		if_id_stall_out : out flag
	);
end entity;

architecture hctrl_impl of Hazard_Control is
begin
	process(id_regs_in, ex_reg_in, mem_reg_in, wb_reg_in) is
		variable stall : flag;
		variable regout : regs_t;
	begin
		stall := '0';
		
		regout := regs_init;

		if (id_regs_in.rd.isRead = '1') then -- da li ID cita
			if (id_regs_in.rd.myNum = ex_reg_in.myNum) then -- da li se brojevi registara poklapaju
				if (ex_reg_in.hazard = '1') then -- da li EX pise u taj registar
					if (ex_reg_in.isRead = '0') then -- ako pise, da li je spreman (EX postavlja ovaj flag na '1' uvek osim za LD	
						stall := '1';
					else
						regout.rd.myValue := ex_reg_in.myValue;
						regout.rd.hazard  := '1';
					end if;
				end if;

			elsif (id_regs_in.rd.myNum = mem_reg_in.myNum and mem_reg_in.hazard = '1') then --da li MEM pise
				regout.rd.myValue := mem_reg_in.myValue;
				regout.rd.hazard  := '1';

			elsif (id_regs_in.rd.myNum = wb_reg_in.myNum and wb_reg_in.hazard = '1') then
				regout.rd.myValue := wb_reg_in.myValue;
				regout.rd.hazard  := '1';
			end if;
		end if;
--TODO: dodati svuda regout.rs1.myNum := ex_reg_in.myNum;
--jer je lakse za pracenje u modelsimu
		--isto za rs1 i rs2
		if (id_regs_in.rs1.isRead = '1') then -- da li ID cita
			if (id_regs_in.rs1.myNum = ex_reg_in.myNum) then -- da li se brojevi registara poklapaju
				if (ex_reg_in.hazard = '1') then -- da li EX pise u taj registar
					if (ex_reg_in.isRead = '0') then -- ako pise, da li je spreman (EX postavlja ovaj flag na '1' uvek osim za LD	
						stall := '1';
					else
						regout.rs1.myValue := ex_reg_in.myValue;
						regout.rs1.hazard  := '1';
					end if;
				end if;

			elsif (id_regs_in.rs1.myNum = mem_reg_in.myNum and mem_reg_in.hazard = '1') then --da li MEM pise
				regout.rs1.myValue := mem_reg_in.myValue;
				regout.rs1.hazard  := '1';

			elsif (id_regs_in.rs1.myNum = wb_reg_in.myNum and wb_reg_in.hazard = '1') then
				regout.rs1.myValue := wb_reg_in.myValue;
				regout.rs1.hazard  := '1';
			end if;
		end if;

		--rs2
		if (id_regs_in.rs2.isRead = '1') then -- da li ID cita
			if (id_regs_in.rs2.myNum = ex_reg_in.myNum) then -- da li se brojevi registara poklapaju
				if (ex_reg_in.hazard = '1') then -- da li EX pise u taj registar
					if (ex_reg_in.isRead = '0') then -- ako pise, da li je spreman (EX postavlja ovaj flag na '1' uvek osim za LD	
						stall := '1';
					else
						regout.rs2.myValue := ex_reg_in.myValue;
						regout.rs2.hazard  := '1';
					end if;
				end if;

			elsif (id_regs_in.rs2.myNum = mem_reg_in.myNum and mem_reg_in.hazard = '1') then --da li MEM pise
				regout.rs2.myValue := mem_reg_in.myValue;
				regout.rs2.hazard  := '1';

			elsif (id_regs_in.rs2.myNum = wb_reg_in.myNum and wb_reg_in.hazard = '1') then
				regout.rs2.myValue := wb_reg_in.myValue;
				regout.rs2.hazard  := '1';
			end if;
		end if;

		if_id_stall_out <= stall;
		id_regs_out <= regout;
	--end if;


	end process;

end architecture;