library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_signed.all;
use ieee.numeric_std.all;
use ieee.std_logic_textio.all;
use work.types.all;
use std.textio.all;

entity DataMem is
	port(
		--reset        : in  flag;
		halt_in      : in  flag;

		--MEM
		mem_addr_in  : in  addr;        --adresa na koju treba da se pise
		mem_word_in  : in  word;        --rec koja treba da se pise u memoriju
		mem_flag_in  : in  flag;        --da li je memorija potrebna
		mem_RW_in    : in  flag;        --0 citanje, 1 pisanje u mem.
		mem_word_out : out word         --rec koja nam je stigla iz memorije
	);
end entity;

architecture DataMem_impl of DataMem is
	--	signal data_cache : data_cache_t;
	--	signal d_bits     : dirty_bits_t;

	impure function init_d_cache return data_cache_t is
		file input_file : text;
		variable input_line : line;
		variable input_addr : addr;
		variable input_data : word;
		variable tmp_mem    : data_cache_t := (others => (others => '0'));
	begin
		file_open(input_file, data_file_path, read_mode);

		loop
			exit when endfile(input_file);

			readline(input_file, input_line);
			hread(input_line, input_addr);
			read(input_line, input_data);

			tmp_mem(to_integer(unsigned(input_addr))) := input_data;

		end loop;

		file_close(input_file);

		return tmp_mem;
	end;

	--moze i bez ovoga ali kao
	impure function init_d_bits return dirty_bits_t is
		variable dirty_bits : dirty_bits_t;
	begin
		dirty_bits := (others => '0');
		return dirty_bits;
	end;

	shared variable data_cache : data_cache_t := init_d_cache;
	shared variable d_bits     : dirty_bits_t := init_d_bits;

	procedure output_d_cache is
		file output_file : text;
		file expected_file : text;
		variable output_line   : line;
		variable expected_line : line;
		variable output_data   : word;
		variable expected_data : word;
		variable output_addr   : addr;
		variable expected_addr : addr;
		--variable data_cache_content : word;
		variable test_result   : integer := 1;
--		variable last_line : integer := 0;
	begin
		file_open(output_file, output_file_path, write_mode);
		file_open(expected_file, expected_file_path, read_mode);

		--		loop
		--			exit when test_result = 0;--endfile(expected_file);

		readline(expected_file, expected_line);
		hread(expected_line, expected_addr);
		read(expected_line, expected_data);

		for i in 0 to data_cache_size - 1 loop
			if (d_bits(i) = '1') then
				if (i /= expected_addr) then
--					write(output_line, string'("nisu iste vrednosti"));--TODO: +"na adresi: "&i
--					writeline(output_file, output_line);
					test_result := 0;
					exit;
				else
					output_addr := expected_addr;
					output_data := data_cache(to_integer(unsigned(expected_addr)));
					
					--TODO: proveriti da li je ok:
					if(expected_data /= output_data) then
						test_result :=0;
						exit;
					end if;					
					hwrite(output_line, output_addr);
					write(output_line, ' ');
					write(output_line, output_data);
					writeline(output_file, output_line);

					exit when endfile(expected_file);
					readline(expected_file, expected_line);
					hread(expected_line, expected_addr);
					read(expected_line, expected_data);
--					if endfile(expected_file) then
--						last_line := 1;
--					end if;
				end if;
			end if;

		end loop;

		--			data_cache_content := data_cache(to_integer(unsigned(input_addr)));
		--
		--			if (input_data /= data_cache_content) then
		--				test_result := 0;
		--			else
		--				if (d_bits(to_integer(unsigned(input_addr))) = '0') then
		--					--d biti za svaku memorijsku lokaciju da znamo da li je uopste bilo upisa u nju
		--					--moze da nam se potrefi da lokacije imaju istu vrednost zato sto su sve nule
		--					--jer smo tako inicijalizovali i kasnije je trebalo da upisemo nulu u tu lokaciju
		--					--a na primer upis se nije desio - tada ne sme da bude test_result = '1'
		--					test_result := 0;
		--				end if;
		--			end if;
		--
		--			if test_result = 0 then
		--				write(output_line, string'("nisu iste vrednosti"));
		--				writeline(output_file, output_line);
		--				exit;
		--			else
		--				output_addr := input_addr;
		--				output_data := data_cache_content;
		--				hwrite(output_line, output_addr);
		--				write(output_line, ' ');
		--				write(output_line, output_data);
		--				writeline(output_file, output_line);
		--			end if;
		--
		--		end loop;
		
--		if not (endfile(expected_file)) or last_line = 1 then
--			test_result := 0;
--		end if;
				
		if test_result = 1 then
			write(output_line, string'("vrednosti su iste"));
			writeline(output_file, output_line);
		else
			write(output_line, string'("nisu iste vrednosti")); --TODO: +"na adresi: "&i
			writeline(output_file, output_line);
		end if;

		file_close(output_file);
		file_close(expected_file);

	end;

begin
	--	process(reset)
	--	begin
	--		if (reset = '1') then
	--			data_cache <= init_d_cache;
	--			d_bits     <= init_d_bits;
	--		end if;
	--	end process;

	process(mem_addr_in, mem_word_in, mem_flag_in, mem_RW_in)
	begin
		mem_word_out <= (others => '0');
		if (mem_flag_in = '1') then
			if (mem_RW_in = '0') then   --citanje (R)
				mem_word_out <= data_cache(to_integer(signed(mem_addr_in)));
			else                        --upis (W)
				data_cache(to_integer(unsigned(mem_addr_in))) := mem_word_in;
				d_bits(to_integer(unsigned(mem_addr_in)))     := '1';
			end if;
		end if;
	end process;

	process(halt_in)
	begin
		if (halt_in = '1') then
			output_d_cache;
		end if;
	end process;

end architecture;