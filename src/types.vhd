library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_textio.all;
use ieee.numeric_std.all;
use std.textio.all;

package types is
	constant word_size : integer := 32;
	subtype word is std_logic_vector(word_size - 1 downto 0);

	constant addr_size : integer := 32;
	subtype addr is std_logic_vector(addr_size - 1 downto 0);

	subtype flag is std_logic;

	constant imm_shift_size : integer := 5;
	subtype imm5 is std_logic_vector(imm_shift_size - 1 downto 0);

	constant imm_value_size : integer := 32;
	subtype imm32 is std_logic_vector(imm_value_size - 1 downto 0);

	constant reg_num_size : integer := 5;
	subtype reg_num is std_logic_vector(reg_num_size - 1 downto 0);

	constant opcode_length : integer := 6;
	subtype opcode_type is std_logic_vector(opcode_length - 1 downto 0);

	constant OPCODE_LOAD  : opcode_type := "000000";
	constant OPCODE_STORE : opcode_type := "000001";
	constant OPCODE_MOV   : opcode_type := "000100";
	constant OPCODE_MOVI  : opcode_type := "000101";
	constant OPCODE_ADD   : opcode_type := "001000";
	constant OPCODE_SUB   : opcode_type := "001001";
	constant OPCODE_ADDI  : opcode_type := "001100";
	constant OPCODE_SUBI  : opcode_type := "001101";
	constant OPCODE_AND   : opcode_type := "010000";
	constant OPCODE_OR    : opcode_type := "010001";
	constant OPCODE_XOR   : opcode_type := "010010";
	constant OPCODE_NOT   : opcode_type := "010011";
	constant OPCODE_SHL   : opcode_type := "011000";
	constant OPCODE_SHR   : opcode_type := "011001";
	constant OPCODE_SAR   : opcode_type := "011010";
	constant OPCODE_ROL   : opcode_type := "011011";
	constant OPCODE_ROR   : opcode_type := "011100";
	constant OPCODE_JMP   : opcode_type := "100000";
	constant OPCODE_JSR   : opcode_type := "100001";
	constant OPCODE_RTS   : opcode_type := "100010";
	constant OPCODE_PUSH  : opcode_type := "100100";
	constant OPCODE_POP   : opcode_type := "100101";
	constant OPCODE_BEQ   : opcode_type := "101000";
	constant OPCODE_BNQ   : opcode_type := "101001";
	constant OPCODE_BGT   : opcode_type := "101010";
	constant OPCODE_BLT   : opcode_type := "101011";
	constant OPCODE_BGE   : opcode_type := "101100";
	constant OPCODE_BLE   : opcode_type := "101101";
	constant OPCODE_HALT  : opcode_type := "111111";
	constant OPCODE_INIT  : opcode_type := "111110";

	--subtype STATE is std_logic_vector(1 downto 0);
	type STATE is (STRONG_NOT_TAKEN, WEAK_NOT_TAKEN, WEAK_TAKEN, STRONG_TAKEN);

	--stack
	constant stack_size : integer := 16;
	type stack is array (0 to stack_size - 1) of word;

	subtype sp_t is integer range -1 to stack_size - 1;

	--registerTable
	constant reg_file_size : integer := 32;
	type reg_file_t is array (0 to reg_file_size - 1) of word;

	type predEntry is record
		addr_from : addr;
		addr_to   : addr;
		state     : STATE;
	end record;

	--tabela prediktora imace npr.2^5 ulaza
	constant pred_table_size : integer := 32;
	type predTable is array (0 to pred_table_size - 1) of predEntry;

	type reg_t is record
		myNum   : reg_num;
		myValue : word;
		isRead  : flag;                 -- Da li ID cita iz tog registra (setuje ID, proverava hctrl)
		-- ili da li WB vrsi upis u taj registar (setuje WB, proverava reg file)
		-- ili da je spreman (setuje EX, proverava hctrl; EX upisuje '1' uvek osim za LD)
		hazard  : flag;                 -- Da li hazard prosledjuje odredjeni registar umesto da se uzme iz reg file-a
	-- (setuje hctrl, proverava ID)
	-- ili da li EX, MEM ili WB trenutno pisu u taj registar (setuju EX, MEM ili WB,
	-- proverava hctrl redom EX -> MEM -> WB)
	end record;

	type regs_t is record
		rd  : reg_t;
		rs1 : reg_t;
		rs2 : reg_t;
	end record;

	type instruction is record
		currentPC   : addr;
		nextPC      : addr;
		branch      : flag;
		flush       : flag;
		halt        : flag;
		opcode      : opcode_type;
		immediate5  : imm5;
		immediate32 : imm32;
		registers   : regs_t;
	--jump        : flag;             --ovde ce nam EX faza upisati da li je do skoka doslo
	end record;

	function init return instruction;
	function regs_init return regs_t;
	impure function read_pc_from_file return addr;

	type if_phase_signals is record
		pred_addr_from_out : addr;
		pred_addr_to_in    : addr;
		ex_flush_in        : flag;
		ex_correct_addr_in : addr;
		hctrl_stall_in     : flag;
		if2id_out          : instruction;
	end record;
	
	constant file_path	: STRING := "E:\vhdl_workspace\vlsi_jun\test\javni_test_inst_in.txt";
	constant data_file_path	: STRING := "E:\vhdl_workspace\vlsi_jun\test\javni_test_data_in.txt";
	constant expected_file_path : STRING := "E:\vhdl_workspace\vlsi_jun\test\javni_test_data_out.txt";
	constant output_file_path : STRING := "E:\vhdl_workspace\vlsi_jun\test\output.txt";
	
	constant instr_cache_size : natural := 2**13;
	--pocetna adresa u javnom testu je 00001000h sto je 2^12 pa cemo napraviti instr cache
	--koji je dovoljno veliki da smesti ovu i naredne vrednosti (ide od 00000000h do 00001FFFh)
	type instr_cache_t is array (natural range 0 to instr_cache_size - 1) of word;
	
	constant data_cache_size : natural := 2**13;
	type data_cache_t is array (natural range 0 to data_cache_size - 1) of word;
	type dirty_bits_t is array (natural range 0 to data_cache_size - 1) of std_logic;
	
end types;

package body types is
	function init return instruction is
		variable instr : instruction;
	begin
		instr.currentPC   := (others => '0');
		instr.nextPC      := (others => '0');
		instr.branch      := '0';
		instr.flush       := '0';
		instr.halt        := '0';
		--instr.jump        := '0';
		instr.opcode      := OPCODE_INIT;
		instr.immediate5  := (others => '0');
		instr.immediate32 := (others => '0');
		
		instr.registers := regs_init;
		
--		instr.registers.rd.myNum   := (others => '0');
--		instr.registers.rd.myValue := (others => '0');
--		instr.registers.rd.isRead  := '0';
--		instr.registers.rd.hazard  := '0';
--
--		instr.registers.rs1.myNum   := (others => '0');
--		instr.registers.rs1.myValue := (others => '0');
--		instr.registers.rs1.isRead  := '0';
--		instr.registers.rs1.hazard  := '0';
--
--		instr.registers.rs2.myNum   := (others => '0');
--		instr.registers.rs2.myValue := (others => '0');
--		instr.registers.rs2.isRead  := '0';
--		instr.registers.rs2.hazard  := '0';

		return instr;
	end function init;

	function regs_init return regs_t is
		variable r : regs_t;
	begin
		r.rd.myNum   := (others => '0');
		r.rd.myValue := (others => '0');
		r.rd.isRead  := '0';
		r.rd.hazard  := '0';

		r.rs1.myNum   := (others => '0');
		r.rs1.myValue := (others => '0');
		r.rs1.isRead  := '0';
		r.rs1.hazard  := '0';

		r.rs2.myNum   := (others => '0');
		r.rs2.myValue := (others => '0');
		r.rs2.isRead  := '0';
		r.rs2.hazard  := '0';
		return r;
	end function;

	impure function read_pc_from_file return addr is
		file input_file : text;
		variable input_line : line;
		variable pc_value   : addr;    -- pc from file
	begin
		file_open(input_file, file_path, read_mode);

		readline(input_file, input_line); --Read the line from the file
		hread(input_line, pc_value);  --Read first word (pc adress)

		file_close(input_file);
		return pc_value;
	end;

end types;