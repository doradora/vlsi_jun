library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_signed.all;
use ieee.numeric_std.all;
use work.types.all;

entity regfile is
	port(
		clk         : in  flag;
		reset       : in  flag;
		
		--ID
		id_regs_in  : in  regs_t;
		id_regs_out : out regs_t;

		--WB
		wb_reg_in   : in  reg_t
	);
end entity;

architecture regfile_impl of regfile is
	signal reg_reg, reg_next : reg_file_t;
begin
	process(clk, reset)
	begin
		if (reset = '1') then
			for i in reg_file_t'range loop
				reg_reg(i) <= std_logic_vector(to_unsigned(i, 32));
			end loop;
		elsif (rising_edge(clk)) then
			reg_reg <= reg_next;
		end if;
	end process;

	process(id_regs_in, reg_reg, wb_reg_in)
		variable myRegs : regs_t;
	begin
		myRegs   := id_regs_in;
		reg_next <= reg_reg;

		if (wb_reg_in.isRead = '1') then --ako WB vrsi upis u taj registar
			reg_next(to_integer(unsigned(wb_reg_in.myNum))) <= wb_reg_in.myValue;
		end if;
		--if (myRegs.rd.hazard = '1') --DODATI JEDNOM PRILIKOM
		myRegs.rd.myValue  := reg_reg(to_integer(unsigned(myRegs.rd.myNum)));
		myRegs.rs1.myValue := reg_reg(to_integer(unsigned(myRegs.rs1.myNum)));
		myRegs.rs2.myValue := reg_reg(to_integer(unsigned(myRegs.rs2.myNum)));
		id_regs_out        <= myRegs;
	end process;

end architecture;