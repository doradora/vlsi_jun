library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_signed.all;
use ieee.numeric_std.all;
use work.types.all;

entity WriteBack is
	port(
		clk                : in  flag;
		reset              : in  flag;

		--HAZARD, da bi radilo prosledjivanje
		hctrl_reg_out      : out reg_t;

		--REG FILE
		regfile_reg_out    : out reg_t;

		--ka prediktoru
		pred_addr_from_out : out addr;
		pred_addr_to_out   : out addr;
		pred_jump_out      : out flag;

		--iz MEM-a
		mem2wb_in          : in  instruction;

		halt_out           : out flag
	);
end entity;

architecture writeback_impl of WriteBack is
	signal instruction_reg, instruction_next : instruction;
begin
	process(clk, reset)
	begin
		if (reset = '1') then
			instruction_reg <= init;
		elsif (rising_edge(clk)) then
			instruction_reg <= instruction_next;
		end if;
	end process;

	process(mem2wb_in, instruction_reg)
		variable instr : instruction;
	begin
		instr := mem2wb_in;

		instruction_next <= instr;

		halt_out <= instr.halt;

	end process;

	regfile_reg_out <= instruction_reg.registers.rd;

	pred_addr_from_out <= instruction_reg.currentPC;
	pred_addr_to_out   <= instruction_reg.nextPC;
	pred_jump_out      <= instruction_reg.branch;

	hctrl_reg_out <= instruction_reg.registers.rd;

end architecture;