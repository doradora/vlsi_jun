library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_signed.all;
use ieee.numeric_std.all;
use work.types.all;

entity Predictor is
	port(
		clk             : in  flag;
		reset           : in  flag;

		-- IF
		if_addr_from_in : in  addr;
		if_addr_to_out  : out addr;
		if_branch_out   : out flag;

		-- WB
		wb_addr_from_in : in  addr;
		wb_addr_to_in   : in  addr;
		wb_jump_in      : in  flag      --da li je doslo do skoka

	);
end entity;

architecture predictor_impl of Predictor is
	signal pred_reg, pred_next : predTable;
begin
	process(clk, reset)
	begin
		if (reset = '1') then
			for i in predTable'range loop
				--na reset popunimo nulama sve
				pred_reg(i).addr_from <= (others => '0'); --std_logic_vector(to_unsigned(0, addr_size));
				pred_reg(i).addr_to   <= (others => '0');
				pred_reg(i).state     <= STRONG_NOT_TAKEN;
			end loop;
		elsif (rising_edge(clk)) then
			pred_reg <= pred_next;
		end if;
	end process;

	process(if_addr_from_in, wb_addr_from_in, wb_addr_to_in, wb_jump_in, pred_reg)
		variable entry_if, entry_wb : integer; --zadnjih pet bitova adrese za ulaz
	begin

		--kad mi stigne signal if_addr_from_in, ja treba da procitam da li imam nesto na toj adresi
		--i da vratim predikciju
		pred_next <= pred_reg;

		entry_if       := to_integer(unsigned(if_addr_from_in(4 downto 0)));
		if_addr_to_out <= pred_reg(entry_if).addr_to;

		--mora da se uklapa cela adresa a ne samo zadnjih 5 bita, koji nam sluze za ulaz
		if (pred_reg(entry_if).addr_from = if_addr_from_in) then
			if (pred_reg(entry_if).state = STRONG_NOT_TAKEN or pred_reg(entry_if).state = WEAK_NOT_TAKEN) then
				if_branch_out <= '0';   --nema skoka
			else
				if_branch_out <= '1';   --skok (adresa je podesena van uslova vec)
			end if;
		else
			if_branch_out <= '0';
		end if;

		--kad od wb stigne:
		entry_wb := to_integer(unsigned(wb_addr_from_in(4 downto 0)));

		--adrese se poklapaju i bilo je skoka, azuriranje stanja
		if (pred_reg(entry_wb).addr_from = wb_addr_from_in) then
			if (wb_jump_in = '1') then
				case pred_reg(entry_wb).state is
					when STRONG_NOT_TAKEN =>
						pred_next(entry_wb).state <= WEAK_NOT_TAKEN;
					when WEAK_NOT_TAKEN =>
						pred_next(entry_wb).state <= WEAK_TAKEN;
					when WEAK_TAKEN =>
						pred_next(entry_wb).state <= STRONG_TAKEN;
					when STRONG_TAKEN =>
						pred_next(entry_wb).state <= STRONG_TAKEN;
					when others =>
						null;
				end case;
			elsif (wb_jump_in = '0') then
				case pred_reg(entry_wb).state is
					when STRONG_NOT_TAKEN =>
						pred_next(entry_wb).state <= STRONG_NOT_TAKEN;
					when WEAK_NOT_TAKEN =>
						pred_next(entry_wb).state <= STRONG_NOT_TAKEN;
					when WEAK_TAKEN =>
						pred_next(entry_wb).state <= WEAK_NOT_TAKEN;
					when STRONG_TAKEN =>
						pred_next(entry_wb).state <= WEAK_TAKEN;
					when others =>
						null;
				end case;
			end if;
		else                            --if (pred_reg(entry_wb).addr_from /= wb_addr_from_in)
			if (wb_jump_in = '1') then
				pred_next(entry_wb).addr_from <= wb_addr_from_in;
				pred_next(entry_wb).addr_to   <= wb_addr_to_in;
				pred_next(entry_wb).state     <= STRONG_TAKEN;
			end if;
		end if;

	end process;
end architecture;