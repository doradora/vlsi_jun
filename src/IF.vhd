library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_signed.all;
use ieee.numeric_std.all;
use work.types.all;

entity InstructionFetch is
	port(
		clk                : in  flag;
		reset              : in  flag;

		--Predictor
		pred_addr_from_out : out addr;
		pred_addr_to_in    : in  addr;
		pred_branch_in     : in  flag;  --predvidjanje da li ima skoka

		--EX
		ex_flush_in        : in  flag;
		ex_correct_addr_in : in  addr;

		--Hazard
		hctrl_stall_in     : in  flag;

		--ID
		if2id_out          : out instruction
	);
end entity;

architecture InstructionFetch_implementation of InstructionFetch is
	signal instruction_reg, instruction_next : instruction;
	signal pc_reg, pc_next                   : addr;
begin
	process(clk, reset) is
	begin
		if (reset = '1') then
			--TODO: citanje iz fajla
			--pc_reg <= (others => '0');
			pc_reg <= read_pc_from_file;
			instruction_reg <= init;
		elsif (rising_edge(clk)) then
			pc_reg          <= pc_next;
			instruction_reg <= instruction_next;
		end if;
	end process;

	process(pred_addr_to_in, pred_branch_in, ex_flush_in, ex_correct_addr_in, hctrl_stall_in, pc_reg, instruction_reg) is
		variable pc    : addr;
		variable instr : instruction;
	begin
		pc    := pc_reg;
		instr := init;

		if (hctrl_stall_in = '1') then
			instr.flush := '1';
		elsif (ex_flush_in = '1') then
			instr.flush := '1';
			pc          := ex_correct_addr_in;
		elsif (pred_branch_in = '1') then
			pc := pred_addr_to_in;
		else
			--pc := pc + 1
			-- pc -> unsigned -> integer
			-- +1
			-- integer -> unsigned -> pc
			pc := std_logic_vector(to_unsigned(to_integer(unsigned(pc)) + 1, addr_size));
		end if;

		instr.currentPC := pc_reg;
		instr.nextPC    := pc;
		instr.branch    := pred_branch_in;

		instruction_next <= instr;
		pc_next          <= pc;

		pred_addr_from_out <= pc;

	end process;

	if2id_out <= instruction_reg;

end architecture;