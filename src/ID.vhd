library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_signed.all;
use ieee.numeric_std.all;
use work.types.all;

entity InstructionDecode is
	port(
		clk                    : in  flag;
		reset                  : in  flag;

		--Instruction Mem
		im_current_pc_out      : out addr;
		im_current_instr_in    : in  word;

		--Reg File
		regfile_regs_in        : in  regs_t;

		--Hazard
		hctrl_regs_in          : in  regs_t;
		hctrl_stall_in         : in  flag;

		--Hazard&Reg File
		regfile_hctrl_regs_out : out regs_t;

		--EX
		ex_flush_in            : in  flag;
		id2ex_out              : out instruction;

		--IF
		if2id_in               : in  instruction
	);
end entity;

architecture InstructionDecode_implementation of InstructionDecode is
	signal instruction_reg, instruction_next : instruction;

begin
	process(clk, reset) is
	begin
		if (reset = '1') then
			instruction_reg <= init;
		elsif (rising_edge(clk)) then
			instruction_reg <= instruction_next;
		end if;
	end process;

	process(im_current_instr_in, hctrl_stall_in, ex_flush_in, regfile_regs_in, hctrl_regs_in, if2id_in, instruction_reg) is
		variable instr     : instruction;
		variable operation : word;
		
		constant zero16 : std_logic_vector(15 downto 0) := (others => '0');
		constant one16 : std_logic_vector(15 downto 0) := (others => '1');
		
	begin
		--init myRegs
		instr := if2id_in;
		
		im_current_pc_out <= instr.currentPC;  --ovo mora ovde
		
		if (hctrl_stall_in = '1' or ex_flush_in = '1' or instr.flush = '1') then
			instr.flush := '1';
			regfile_hctrl_regs_out <= regs_init;
		else
			operation    := im_current_instr_in;
			instr.opcode := operation(31 downto 26);
			--TODO: ovde nastaje problem koji gazi onaj nas OPCODE_INIT i setuje opcode na 000000
			--zbog tih nula se posle desava da u mem fazi on istripuje da je u pitanju load instrukcija
			--i setuje fleg za citanje iz memorije na 1 i dovuce podatak sa adrese 1 koji je 100 

			case instr.opcode is
				when OPCODE_LOAD | OPCODE_ADDI | OPCODE_SUBI =>
					instr.registers.rd.myNum   := operation(25 downto 21);
					instr.registers.rs1.myNum  := operation(20 downto 16);
					instr.registers.rs1.isRead := '1';
					if (operation(15) = '1') then
						instr.immediate32 := one16 & operation(15 downto 0);
					else
						instr.immediate32 := zero16 & operation(15 downto 0);
					end if;

				when OPCODE_ADD | OPCODE_SUB | OPCODE_AND | OPCODE_OR | OPCODE_XOR | OPCODE_NOT =>
					instr.registers.rd.myNum   := operation(25 downto 21);
					instr.registers.rs1.myNum  := operation(20 downto 16);
					instr.registers.rs1.isRead := '1';
					instr.registers.rs2.myNum  := operation(15 downto 11);
					instr.registers.rs2.isRead := '1';

				when OPCODE_MOV =>
					instr.registers.rd.myNum   := operation(25 downto 21);
					instr.registers.rs1.myNum  := operation(20 downto 16);
					instr.registers.rs1.isRead := '1';

				when OPCODE_MOVI =>
					instr.registers.rd.myNum := operation(25 downto 21);
					if (operation(15) = '1') then
						instr.immediate32 := one16 & operation(15 downto 0);
					else
						instr.immediate32 := zero16 & operation(15 downto 0);
					end if;
					--instr.immediate32        := operation(15 downto 0);

				when OPCODE_JSR | OPCODE_JMP =>
					instr.registers.rs1.myNum  := operation(20 downto 16);
					instr.registers.rs1.isRead := '1';
					if (operation(15) = '1') then
						instr.immediate32 := one16 & operation(15 downto 0);
					else
						instr.immediate32 := zero16 & operation(15 downto 0);
					end if;					
					--instr.immediate32          := operation(15 downto 0);

				when OPCODE_SHL | OPCODE_SHR | OPCODE_SAR | OPCODE_ROL | OPCODE_ROR =>
					instr.registers.rd.myNum  := operation(25 downto 21);
					instr.registers.rd.isRead := '1';
					instr.immediate5          := operation(15 downto 11);

				when OPCODE_STORE | OPCODE_BEQ | OPCODE_BNQ | OPCODE_BGT | OPCODE_BLT | OPCODE_BGE | OPCODE_BLE =>
					if (operation(25) = '1') then
						instr.immediate32 := one16 & operation(25 downto 21) & operation(10 downto 0);
					else
						instr.immediate32 := zero16 & operation(25 downto 21) & operation(10 downto 0);
					end if;
					--instr.immediate32          := operation(25 downto 21) & operation(10 downto 0);
					--test: da li ovo redja lepo bite od 0 do 16
					instr.registers.rs1.myNum  := operation(20 downto 16);
					instr.registers.rs1.isRead := '1';
					instr.registers.rs2.myNum  := operation(15 downto 11);
					instr.registers.rs2.isRead := '1';

				when OPCODE_POP =>
					instr.registers.rd.myNum := operation(25 downto 21);

				when OPCODE_PUSH =>
					instr.registers.rs1.myNum  := operation(20 downto 16);
					instr.registers.rs1.isRead := '1';

				when OPCODE_RTS =>
					null;

				when OPCODE_HALT =>
					instr.halt := '1';
				
				when OPCODE_INIT =>
					instr.registers := regs_init;
					
				when others =>
					instr.halt := '1';
			end case;

						--izlaz ka Hazard Ctrl i ka Reg File u isto vreme
			regfile_hctrl_regs_out <= instr.registers;

			--rs1
			if (hctrl_regs_in.rs1.hazard = '1') then
				instr.registers.rs1.myValue := hctrl_regs_in.rs1.myValue;
			else
				instr.registers.rs1.myValue := regfile_regs_in.rs1.myValue;
			end if;

			--rs2
			if (hctrl_regs_in.rs2.hazard = '1') then
				instr.registers.rs2.myValue := hctrl_regs_in.rs2.myValue;
			else
				instr.registers.rs2.myValue := regfile_regs_in.rs2.myValue;
			end if;

			--rd
			if (hctrl_regs_in.rd.hazard = '1') then
				instr.registers.rd.myValue := hctrl_regs_in.rd.myValue;
			else
				instr.registers.rd.myValue := regfile_regs_in.rd.myValue;
			end if;

		end if;
		
--		if (hctrl_stall_in = '1' or ex_flush_in = '1' or instr.flush = '1') then
--			--regfile_hctrl_regs_out <= regs_init;
--			regfile_hctrl_regs_out <= instr.registers;
--		else
--			--regfile_hctrl_regs_out <= instr.registers;
--			regfile_hctrl_regs_out <= regs_init;
--		end if;
		
		instruction_next <= instr;
	end process;

	id2ex_out <= instruction_reg;

end architecture;