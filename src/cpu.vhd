library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.types.all;

entity CPU is
	port(
		clk_in      : in  flag;
		reset       : in  flag;

		--linije za komunikaciju sa memorijama:		
		--Instruction Cache
		im_addr_out : out addr;
		im_data_in  : in  word;

		--Data Cache
		dm_addr_out : out addr;
		dm_data_out : out word;
		dm_data_in  : in  word;
		dm_RW_out   : out flag;
		dm_flag_out : out flag;
		
		halt_dm : out flag
	---------------------------------------		
	);
end entity;

architecture CPU_impl of CPU is
	signal clk                : flag;
	signal halt               : flag;
	signal if_addr_from_pred  : addr;
	signal pred_addr_to_if    : addr;
	signal pred_branch_if     : flag;
	signal ex_flush_if_id     : flag;
	signal ex_correct_addr_if : addr;
	signal hctrl_stall_if_id  : flag;
	signal if2id              : instruction;

	signal regfile_regs_id       : regs_t;
	signal hctrl_regs_id         : regs_t;
	signal id_regs_regfile_hctrl : regs_t;
	signal id2ex                 : instruction;

	signal ex2mem       : instruction;
	signal ex_reg_hctrl : reg_t;

	signal wb_addr_from_pred : addr;
	signal wb_addr_to_pred   : addr;
	signal wb_jump_pred      : flag;

	signal mem_reg_hctrl : reg_t;
	signal wb_reg_hctrl  : reg_t;

	signal wb_reg_regfile : reg_t;

	signal mem2wb : instruction;

begin
	COMP_IF : entity work.InstructionFetch
		port map(
			clk                => clk,
			reset              => reset,
			pred_addr_from_out => if_addr_from_pred,
			pred_addr_to_in    => pred_addr_to_if,
			pred_branch_in     => pred_branch_if,
			ex_flush_in        => ex_flush_if_id,
			ex_correct_addr_in => ex_correct_addr_if,
			hctrl_stall_in     => hctrl_stall_if_id,
			if2id_out          => if2id
		);
	COMP_ID : entity work.InstructionDecode
		port map(
			clk                    => clk,
			reset                  => reset,
			im_current_pc_out      => im_addr_out,
			im_current_instr_in    => im_data_in,
			regfile_regs_in        => regfile_regs_id,
			hctrl_regs_in          => hctrl_regs_id,
			hctrl_stall_in         => hctrl_stall_if_id,
			regfile_hctrl_regs_out => id_regs_regfile_hctrl,
			ex_flush_in            => ex_flush_if_id,
			id2ex_out              => id2ex,
			if2id_in               => if2id
		);
	COMP_EX : entity work.Execute
		port map(
			clk                 => clk,
			reset               => reset,
			id2ex_in            => id2ex,
			if_correct_addr_out => ex_correct_addr_if,
			if_id_flush_out     => ex_flush_if_id,
			ex2mem_out          => ex2mem,
			hctrl_reg_out       => ex_reg_hctrl
		);
	COMP_PRED : entity work.Predictor
		port map(
			clk             => clk,
			reset           => reset,
			if_addr_from_in => if_addr_from_pred,
			if_addr_to_out  => pred_addr_to_if,
			if_branch_out   => pred_branch_if,
			wb_addr_from_in => wb_addr_from_pred,
			wb_addr_to_in   => wb_addr_to_pred,
			wb_jump_in      => wb_jump_pred
		);
	COMP_HCTRL : entity work.Hazard_Control
		port map(
			id_regs_in      => id_regs_regfile_hctrl,
			id_regs_out     => hctrl_regs_id,
			ex_reg_in       => ex_reg_hctrl,
			mem_reg_in      => mem_reg_hctrl,
			wb_reg_in       => wb_reg_hctrl,
			if_id_stall_out => hctrl_stall_if_id
		);
	COMP_REGF : entity work.regfile
		port map(
			clk         => clk,
			reset       => reset,
			id_regs_in  => id_regs_regfile_hctrl,
			id_regs_out => regfile_regs_id,
			wb_reg_in   => wb_reg_regfile
		);
	COMP_MEM : entity work.Mem
		port map(
			clk           => clk,
			reset         => reset,
			hctrl_reg_out => mem_reg_hctrl,
			dm_addr_out   => dm_addr_out,
			dm_word_out   => dm_data_out,
			dm_flag_out   => dm_flag_out,
			dm_RW_out     => dm_RW_out,
			dm_word_in    => dm_data_in,
			ex2mem_in     => ex2mem,
			mem2wb_out    => mem2wb
		);
	COMP_WB : entity work.WriteBack
		port map(
			clk                => clk,
			reset              => reset,
			hctrl_reg_out      => wb_reg_hctrl,
			regfile_reg_out    => wb_reg_regfile,
			pred_addr_from_out => wb_addr_from_pred,
			pred_addr_to_out   => wb_addr_to_pred,
			pred_jump_out      => wb_jump_pred,
			mem2wb_in          => mem2wb,
			halt_out           => halt
		);

	halt_dm <= halt;
	clk <= clk_in when halt /= '1' else '0';
	
end architecture;