library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_signed.all;
use ieee.numeric_std.all;
use work.types.all;

entity Execute is
	port(
		clk                 : in  flag;
		reset               : in  flag;

		--ID
		id2ex_in            : in  instruction;

		--IF
		if_correct_addr_out : out addr;

		--IF&ID
		if_id_flush_out     : out flag;

		--MEM
		ex2mem_out          : out instruction;

		--Hazard
		hctrl_reg_out       : out reg_t

	--dm_instr_out      : out instruction
	);
end entity Execute;

architecture EX_IMPL of Execute is
	signal instruction_reg, instruction_next : instruction;
	signal stack_reg, stack_next             : stack;
	signal sp_reg, sp_next                   : sp_t;
--shared variable instr                           : instruction;

begin
	process(clk, reset) is
	begin
		if (reset = '1') then
			instruction_reg <= init;
			sp_reg          <= stack_size - 1;
			for i in stack'range loop
				stack_reg(i) <= (others => '0');
			end loop;
		--inicijalizacija steka
		-- sp_reg := stack_size - 1 na pocetku (empty descending)
		elsif (rising_edge(clk)) then
			instruction_reg <= instruction_next;
			sp_reg          <= sp_next;
			stack_reg       <= stack_next;
		end if;
	end process;

	process(instruction_reg, stack_reg, sp_reg, id2ex_in) is
		variable instr : instruction;

		procedure push(w : in word) is
		begin
			if (sp_reg = -1) then       --pokazuje na prvu slobodnu
				instr.halt := '1';      --pun stek znaci da je sp_reg = -1
			else
				stack_next(sp_reg) <= w;
				sp_next            <= sp_reg - 1;
			end if;
		end procedure;

		impure function pop return word is
		begin
			if (sp_reg = stack_size - 1) then
				return (others => '0'); --prazan stek
			else
				sp_next <= sp_reg + 1;
				return stack_reg(sp_reg + 1);
			end if;
		end function;
--mislim da postoji problem ovde sa negativnim vrednostima pri racunanju pc-ja
--primer iz testa BGT R5, R4, -2
--1005h+(-2) = 1 1003h ??
		impure function add_address(a, b : in addr) return addr is
			-- funkcija koja zavisi od bilo cega osim ulaznih parametara je "necista" (impure)
			variable overflow_test : std_logic_vector(addr_size downto 0); --33 bita
		begin
			overflow_test := std_logic_vector(to_unsigned(to_integer(signed(a)) + to_integer(signed(b)), addr_size + 1));
			if (overflow_test(addr_size) = '1') then --33ci bit = '1', od 0 do 32
				instr.halt := '1';
			end if;
			return overflow_test(addr_size - 1 downto 0);
		end function;

		function add(a, b : in word) return word is
		begin
			return std_logic_vector(to_signed(to_integer(signed(a)) + to_integer(signed(b)), word_size));
		end function;

		function sub(a, b : in word) return word is
		begin
			return std_logic_vector(to_signed(to_integer(signed(a)) - to_integer(signed(b)), word_size));
		end function;

		variable Rd    : reg_t;
		variable Rs1   : reg_t;
		variable Rs2   : reg_t;
		variable Imm32 : imm32;

		variable flush_to_if_id : flag;

--		constant zero16 : std_logic_vector(15 downto 0) := (others => '0');
--		constant one16 : std_logic_vector(15 downto 0) := (others => '1');

	begin
		instr := id2ex_in;
		Rd    := instr.registers.rd;
		Rs1   := instr.registers.rs1;
		Rs2   := instr.registers.rs2;
		Imm32 := instr.immediate32;

		flush_to_if_id := '0';

		--?
		sp_next    <= sp_reg;
		stack_next <= stack_reg;
		--?

		if (instr.flush /= '1') then
			Rd.hazard := '0';
			Rd.isRead := '1';
			

			case (instr.opcode) is
				when OPCODE_LOAD =>
					-- adresa memorije se privremeno smesta u Rd
					Rd.myValue := add_address(Rs1.myValue, Imm32);
					Rd.hazard  := '1';  --instrukcija pise u registar
					Rd.isRead  := '0';  --podatak nije spreman (LOAD)

				when OPCODE_STORE =>
					--napomena: DATA_MEM je asinhrona
					Rd.myValue := add_address(Rs1.myValue, Imm32);
					Rd.hazard  := '0';  --instrukcija ne pise u registar
					--Rd registar ne postoji u store

				when OPCODE_MOV =>
					Rd.myValue := Rs1.myValue;
					Rd.hazard  := '1';

				when OPCODE_MOVI =>
					Rd.myValue := Imm32;
					Rd.hazard  := '1';

				when OPCODE_ADD =>
					Rd.myValue := add(Rs1.myValue, Rs2.myValue);
					Rd.hazard  := '1';

				when OPCODE_SUB =>
					Rd.myValue := sub(Rs1.myValue, Rs2.myValue);
					Rd.hazard  := '1';

				when OPCODE_ADDI =>
					Rd.myValue := add(Rs1.myValue, Imm32);
					Rd.hazard  := '1';

				when OPCODE_SUBI =>
					Rd.myValue := sub(Rs1.myValue, Imm32);
					Rd.hazard  := '1';

				when OPCODE_AND =>
					Rd.myValue := Rs1.myValue and Rs2.myValue;
					Rd.hazard  := '1';

				when OPCODE_OR =>
					Rd.myValue := Rs1.myValue or Rs2.myValue;
					Rd.hazard  := '1';

				when OPCODE_XOR =>
					Rd.myValue := Rs1.myValue xor Rs2.myValue;
					Rd.hazard  := '1';

				when OPCODE_NOT =>
					Rd.myValue := not Rs1.myValue;
					Rd.hazard  := '1';

				when OPCODE_SHL =>
					Rd.myValue := std_logic_vector(unsigned(Rd.myValue) sll to_integer(unsigned(instr.immediate5)));
					Rd.hazard  := '1';

				when OPCODE_SHR =>
					Rd.myValue := std_logic_vector(unsigned(Rd.myValue) srl to_integer(unsigned(instr.immediate5)));
					Rd.hazard  := '1';

				when OPCODE_SAR =>
					Rd.myValue := std_logic_vector(shift_right(signed(Rd.myValue), to_integer(unsigned(instr.immediate5))));
					--Rd.myValue := std_logic_vector(signed(Rd.myValue) sra to_integer(unsigned(instr.immediate5)));
					Rd.hazard  := '1';

				when OPCODE_ROL =>
					Rd.myValue := std_logic_vector(unsigned(Rd.myValue) rol to_integer(unsigned(instr.immediate5)));
					Rd.hazard  := '1';
					
				when OPCODE_ROR =>
					Rd.myValue := std_logic_vector(unsigned(Rd.myValue) ror to_integer(unsigned(instr.immediate5)));
					Rd.hazard  := '1';
					
				when OPCODE_JMP =>
					--uvek je trebalo skociti
					Rd.myValue := add_address(Rs1.myValue, Imm32); --adresa skoka (pomeraj u odnosu na PC)
					if (instr.branch = '0') then
						instr.branch   := '1';
						flush_to_if_id := '1';
						instr.nextPC   := add_address(instr.currentPC, Rd.myValue); --wb javlja prediktoru
					end if;
					Rd.isRead := '0';

				when OPCODE_JSR =>
					--
					push(instr.currentPC);
					Rd.myValue := add_address(Rs1.myValue, Imm32);

					if (instr.branch = '0') then
						instr.branch   := '1';
						flush_to_if_id := '1';
						instr.nextPC   := add_address(instr.currentPC, Rd.myValue); --wb javlja prediktoru
					end if;
					Rd.isRead := '0';

				when OPCODE_RTS =>
					flush_to_if_id := '1';
					instr.nextPC   := pop; --UVEK javlja IF-u

				when OPCODE_PUSH =>
					push(Rs1.myValue);

				when OPCODE_POP =>
					Rd.myValue := pop;
					Rd.hazard  := '1';

				when OPCODE_BEQ =>
					Rd.myValue := sub(Rs1.myValue, Rs2.myValue);
					--ostali skokovi samo drugacije racunaju uslov (Rd!=0, Rd>0, ...)
					if (to_integer(unsigned(Rd.myValue)) = 0) then
						if (instr.branch = '0') then
							--treba skok a bilo je predvidjeno da nece biti
							instr.branch   := '1';
							flush_to_if_id := '1';
							instr.nextPC   := add_address(instr.currentPC, Imm32); --wb javlja prediktoru
						--ne treba nam else jer ako je bio predvidjen skok, onda je sve regularno
						end if;
					else
						if (instr.branch = '1') then
							--ne treba skok, a predvidjeno je da treba, znaci moramo flush i da vratimo pravu adresu
							instr.branch   := '0';
							flush_to_if_id := '1'; --moramo da flushujemo
							instr.nextPC   := std_logic_vector(to_unsigned(to_integer(unsigned(instr.currentPC)) + 1, addr_size));
						--u suprotnom, ne treba intervenisati
						end if;
					end if;
					Rd.isRead := '0';

				when OPCODE_BNQ =>
					Rd.myValue := sub(Rs1.myValue, Rs2.myValue);

					if (to_integer(unsigned(Rd.myValue)) /= 0) then
						if (instr.branch = '0') then
							instr.branch   := '1';
							flush_to_if_id := '1';
							instr.nextPC   := add_address(instr.currentPC, Imm32);
						end if;
					else
						if (instr.branch = '1') then
							instr.branch   := '0';
							flush_to_if_id := '1';
							instr.nextPC   := std_logic_vector(to_unsigned(to_integer(unsigned(instr.currentPC)) + 1, addr_size));
						end if;
					end if;
					Rd.isRead := '0';

				when OPCODE_BGT =>
					Rd.myValue := sub(Rs1.myValue, Rs2.myValue);
					if (to_integer(unsigned(Rd.myValue)) > 0) then--signed?
						if (instr.branch = '0') then
							instr.branch   := '1';
							flush_to_if_id := '1';
							instr.nextPC   := add_address(instr.currentPC, Imm32);
						end if;
					else
						if (instr.branch = '1') then
							instr.branch   := '0';
							flush_to_if_id := '1';
							instr.nextPC   := std_logic_vector(to_unsigned(to_integer(unsigned(instr.currentPC)) + 1, addr_size));
						end if;
					end if;
					Rd.isRead := '0';

				when OPCODE_BLT =>
					Rd.myValue := sub(Rs1.myValue, Rs2.myValue);
					if (to_integer(unsigned(Rd.myValue)) < 0) then
						if (instr.branch = '0') then
							instr.branch   := '1';
							flush_to_if_id := '1';
							instr.nextPC   := add_address(instr.currentPC, Imm32);
						end if;
					else
						if (instr.branch = '1') then
							instr.branch   := '0';
							flush_to_if_id := '1';
							instr.nextPC   := std_logic_vector(to_unsigned(to_integer(unsigned(instr.currentPC)) + 1, addr_size));
						end if;
					end if;
					Rd.isRead := '0';

				when OPCODE_BGE =>
					Rd.myValue := sub(Rs1.myValue, Rs2.myValue);
					if (to_integer(unsigned(Rd.myValue)) >= 0) then
						if (instr.branch = '0') then
							instr.branch   := '1';
							flush_to_if_id := '1';
							instr.nextPC   := add_address(instr.currentPC, Imm32);
						end if;
					else
						if (instr.branch = '1') then
							instr.branch   := '0';
							flush_to_if_id := '1';
							instr.nextPC   := std_logic_vector(to_unsigned(to_integer(unsigned(instr.currentPC)) + 1, addr_size));
						end if;
					end if;
					Rd.isRead := '0';

				when OPCODE_BLE =>
					Rd.myValue := sub(Rs1.myValue, Rs2.myValue);
					if (to_integer(unsigned(Rd.myValue)) <= 0) then
						if (instr.branch = '0') then
							instr.branch   := '1';
							flush_to_if_id := '1';
							instr.nextPC   := add_address(instr.currentPC, Imm32);
						end if;
					else
						if (instr.branch = '1') then
							instr.branch   := '0';
							flush_to_if_id := '1';
							instr.nextPC   := std_logic_vector(to_unsigned(to_integer(unsigned(instr.currentPC)) + 1, addr_size));
						end if;
					end if;
					Rd.isRead := '0';

				when OPCODE_HALT =>
					--razmisli sta se desava tada!
					--ne radi lepo
					instr.halt     := '1';
					instr.flush    := '1';
					flush_to_if_id := '1';
					instr.nextPC   := (others => '-'); --nedefinisana vrednost
					
				when others =>
					null;
			end case;
		end if;

		instr.registers.rd := Rd;

		if_id_flush_out     <= flush_to_if_id;
		if_correct_addr_out <= instr.nextPC;
		hctrl_reg_out       <= Rd;

		--to _next
		instruction_next <= instr;

	end process;

	ex2mem_out <= instruction_reg;

end architecture;