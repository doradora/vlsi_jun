onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -radix hexadecimal /projekat/COMP_CPU/clk_in
add wave -noupdate -radix hexadecimal /projekat/COMP_CPU/reset
add wave -noupdate -radix hexadecimal /projekat/COMP_CPU/im_addr_out
add wave -noupdate -radix hexadecimal -childformat {{/projekat/COMP_CPU/im_data_in(31) -radix hexadecimal} {/projekat/COMP_CPU/im_data_in(30) -radix hexadecimal} {/projekat/COMP_CPU/im_data_in(29) -radix hexadecimal} {/projekat/COMP_CPU/im_data_in(28) -radix hexadecimal} {/projekat/COMP_CPU/im_data_in(27) -radix hexadecimal} {/projekat/COMP_CPU/im_data_in(26) -radix hexadecimal} {/projekat/COMP_CPU/im_data_in(25) -radix hexadecimal} {/projekat/COMP_CPU/im_data_in(24) -radix hexadecimal} {/projekat/COMP_CPU/im_data_in(23) -radix hexadecimal} {/projekat/COMP_CPU/im_data_in(22) -radix hexadecimal} {/projekat/COMP_CPU/im_data_in(21) -radix hexadecimal} {/projekat/COMP_CPU/im_data_in(20) -radix hexadecimal} {/projekat/COMP_CPU/im_data_in(19) -radix hexadecimal} {/projekat/COMP_CPU/im_data_in(18) -radix hexadecimal} {/projekat/COMP_CPU/im_data_in(17) -radix hexadecimal} {/projekat/COMP_CPU/im_data_in(16) -radix hexadecimal} {/projekat/COMP_CPU/im_data_in(15) -radix hexadecimal} {/projekat/COMP_CPU/im_data_in(14) -radix hexadecimal} {/projekat/COMP_CPU/im_data_in(13) -radix hexadecimal} {/projekat/COMP_CPU/im_data_in(12) -radix hexadecimal} {/projekat/COMP_CPU/im_data_in(11) -radix hexadecimal} {/projekat/COMP_CPU/im_data_in(10) -radix hexadecimal} {/projekat/COMP_CPU/im_data_in(9) -radix hexadecimal} {/projekat/COMP_CPU/im_data_in(8) -radix hexadecimal} {/projekat/COMP_CPU/im_data_in(7) -radix hexadecimal} {/projekat/COMP_CPU/im_data_in(6) -radix hexadecimal} {/projekat/COMP_CPU/im_data_in(5) -radix hexadecimal} {/projekat/COMP_CPU/im_data_in(4) -radix hexadecimal} {/projekat/COMP_CPU/im_data_in(3) -radix hexadecimal} {/projekat/COMP_CPU/im_data_in(2) -radix hexadecimal} {/projekat/COMP_CPU/im_data_in(1) -radix hexadecimal} {/projekat/COMP_CPU/im_data_in(0) -radix hexadecimal}} -subitemconfig {/projekat/COMP_CPU/im_data_in(31) {-height 15 -radix hexadecimal} /projekat/COMP_CPU/im_data_in(30) {-height 15 -radix hexadecimal} /projekat/COMP_CPU/im_data_in(29) {-height 15 -radix hexadecimal} /projekat/COMP_CPU/im_data_in(28) {-height 15 -radix hexadecimal} /projekat/COMP_CPU/im_data_in(27) {-height 15 -radix hexadecimal} /projekat/COMP_CPU/im_data_in(26) {-height 15 -radix hexadecimal} /projekat/COMP_CPU/im_data_in(25) {-height 15 -radix hexadecimal} /projekat/COMP_CPU/im_data_in(24) {-height 15 -radix hexadecimal} /projekat/COMP_CPU/im_data_in(23) {-height 15 -radix hexadecimal} /projekat/COMP_CPU/im_data_in(22) {-height 15 -radix hexadecimal} /projekat/COMP_CPU/im_data_in(21) {-height 15 -radix hexadecimal} /projekat/COMP_CPU/im_data_in(20) {-height 15 -radix hexadecimal} /projekat/COMP_CPU/im_data_in(19) {-height 15 -radix hexadecimal} /projekat/COMP_CPU/im_data_in(18) {-height 15 -radix hexadecimal} /projekat/COMP_CPU/im_data_in(17) {-height 15 -radix hexadecimal} /projekat/COMP_CPU/im_data_in(16) {-height 15 -radix hexadecimal} /projekat/COMP_CPU/im_data_in(15) {-height 15 -radix hexadecimal} /projekat/COMP_CPU/im_data_in(14) {-height 15 -radix hexadecimal} /projekat/COMP_CPU/im_data_in(13) {-height 15 -radix hexadecimal} /projekat/COMP_CPU/im_data_in(12) {-height 15 -radix hexadecimal} /projekat/COMP_CPU/im_data_in(11) {-height 15 -radix hexadecimal} /projekat/COMP_CPU/im_data_in(10) {-height 15 -radix hexadecimal} /projekat/COMP_CPU/im_data_in(9) {-height 15 -radix hexadecimal} /projekat/COMP_CPU/im_data_in(8) {-height 15 -radix hexadecimal} /projekat/COMP_CPU/im_data_in(7) {-height 15 -radix hexadecimal} /projekat/COMP_CPU/im_data_in(6) {-height 15 -radix hexadecimal} /projekat/COMP_CPU/im_data_in(5) {-height 15 -radix hexadecimal} /projekat/COMP_CPU/im_data_in(4) {-height 15 -radix hexadecimal} /projekat/COMP_CPU/im_data_in(3) {-height 15 -radix hexadecimal} /projekat/COMP_CPU/im_data_in(2) {-height 15 -radix hexadecimal} /projekat/COMP_CPU/im_data_in(1) {-height 15 -radix hexadecimal} /projekat/COMP_CPU/im_data_in(0) {-height 15 -radix hexadecimal}} /projekat/COMP_CPU/im_data_in
add wave -noupdate -radix hexadecimal /projekat/COMP_CPU/dm_addr_out
add wave -noupdate -radix hexadecimal /projekat/COMP_CPU/dm_data_out
add wave -noupdate -radix hexadecimal /projekat/COMP_CPU/dm_data_in
add wave -noupdate -radix hexadecimal /projekat/COMP_CPU/dm_RW_out
add wave -noupdate -radix hexadecimal /projekat/COMP_CPU/dm_flag_out
add wave -noupdate -radix hexadecimal /projekat/COMP_CPU/clk
add wave -noupdate -radix hexadecimal /projekat/COMP_CPU/halt
add wave -noupdate -radix hexadecimal /projekat/COMP_CPU/if_addr_from_pred
add wave -noupdate -radix hexadecimal /projekat/COMP_CPU/pred_addr_to_if
add wave -noupdate -radix hexadecimal /projekat/COMP_CPU/pred_branch_if
add wave -noupdate -radix hexadecimal /projekat/COMP_CPU/ex_flush_if_id
add wave -noupdate -radix hexadecimal /projekat/COMP_CPU/ex_correct_addr_if
add wave -noupdate -radix hexadecimal /projekat/COMP_CPU/hctrl_stall_if_id
add wave -noupdate -radix hexadecimal /projekat/COMP_CPU/if2id
add wave -noupdate -radix hexadecimal /projekat/COMP_CPU/regfile_regs_id
add wave -noupdate -radix hexadecimal /projekat/COMP_CPU/hctrl_regs_id
add wave -noupdate -radix hexadecimal /projekat/COMP_CPU/id_regs_regfile_hctrl
add wave -noupdate -radix hexadecimal /projekat/COMP_CPU/id2ex
add wave -noupdate -radix hexadecimal /projekat/COMP_CPU/ex2mem
add wave -noupdate -radix hexadecimal /projekat/COMP_CPU/ex_reg_hctrl
add wave -noupdate -radix hexadecimal /projekat/COMP_CPU/wb_addr_from_pred
add wave -noupdate -radix hexadecimal /projekat/COMP_CPU/wb_addr_to_pred
add wave -noupdate -radix hexadecimal /projekat/COMP_CPU/wb_jump_pred
add wave -noupdate -radix hexadecimal /projekat/COMP_CPU/mem_reg_hctrl
add wave -noupdate -radix hexadecimal /projekat/COMP_CPU/wb_reg_hctrl
add wave -noupdate -radix hexadecimal /projekat/COMP_CPU/wb_reg_regfile
add wave -noupdate -radix hexadecimal /projekat/COMP_CPU/mem2wb
add wave -noupdate /projekat/COMP_DC/d_bits
add wave -noupdate /projekat/COMP_DC/data_cache
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {25000 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 150
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ns
update
WaveRestoreZoom {0 ps} {239705 ps}
